#include "PWRM.h"

uint32_t minutes = 0;
uint8_t seconds = 0;
uint8_t action = 1;

void setup() {
  Serial.begin(115200);
  delay(5000);
  initAnalogMeasurements();
  initPowerManager();

  // Enable WDG to wake up every second and run ISR
  WDTCSR = 24;
  WDTCSR = 6;
  WDTCSR |= (1 << 6);

  // Enable deep sleep mode
  SMCR |= (1 << 2);
}

void loop() {
  static int i = 0;
  //Serial.println(++i);
  if(action){
    action--;
    // Order is important. Get values, check errors and then do some stuff
    updateAnalogValues();
    errorControl();
    chargeManager();
    if(getChargingState() == UNDER_TEST)
      action++;
//    sprint("State of charge", (int)getChargingState());
//    sprint("Bat OV", (int)batteryOvervoltage());
//    sprint("Bat UV", (int)batteryUndervoltage());
//    sprint("PIR UV", (int)pirUndervoltage());
//    sprint("MCU UV", (int)mcuUndervoltage());
//    sprint("Low temp", (int)lowTemperature());
//    sprint("High temp", (int)highTemperature());
//    sprint("Vcc", getVcc());
//    sprint("Vbat", getVbat());
//    sprint("Vsol", getVsol());
//    sprint("Temp", getTemperature());
//    sprint("ADC", (int)getAnalogValue());
//    Serial.println();
  }
//  delay(100);

  SMCR |= 1;                      // Enable sleep mode before going to sleep
  __asm__ __volatile__("sleep");  // In line assembler to go to sleep
  SMCR &= ~1;                     // Disable sleep after wake up
}

ISR(WDT_vect) {

  if(++seconds >= 5){
    seconds = 0;
    minutes++;
    action++;
  }
}

void sprint(String s, int val){
  Serial.print(s);
  Serial.print(": ");
  Serial.println(val);
}
