#ifndef PWRM_H
#define PWRM_H

#include <Arduino.h>
#include <AM.h>
#include "externals\config.h"
/**
 * Vcc min 2,8V Vbat 3,2V
 * Vbat min 3,65V in order to have Vcc 3,3V
 */

enum StateOfCharge : uint8_t
{
  DISABLED,
  UNDER_TEST,
  CHARGING,
  TEST_FAIED
};

#define DELAY_BETWEEN_SOLAR_TESTS_MIN 10
 
#define SET_SOLAR_VOLTAGE_MV          5000 //Sufficient voltage to produce 10mA
#define MIN_DELTA_SOL_BATT_VOLTAGE_MV 690   // 700mV drop at diode represents around 10mA of current

#define SET_NIGHT_REGIME_VOLTAGE_MV   3500
#define RESET_NIGHT_REGIME_VOLTAGE_MV 4000

#define SET_CHARGING_VOLTAGE_MV       3850
#define RESET_CHARGING_VOLTAGE_MV     4100
#define SET_BATT_OVERVOLTAGE_MV       4200
#define RESET_BATT_OVERVOLTAGE_MV     4150
#define SET_BATT_UNDERVOLTAGE_MV      3600 //All functionalities are disabled
#define RESET_BATT_UNDERVOLTAGE_MV    3650
#define SET_PIR_UNDERVOLTAGE_MV       3780 //Unreliable PIR sensor reading
#define RESET_PIR_UNDERVOLTAGE_MV     3800
#define SET_MCU_UNDERVOLTAGE_MV       2900 //Little above unstable voltage for operation
#define RESET_MCU_UNDERVOLTAGE_MV     3950
#define SET_LOW_TEMPERATURE_DEGC      5
#define RESET_LOW_TEMPERATURE_DEGC    8
#define SET_HIGH_TEMPERATURE_DEGC     45
#define RESET_HIGH_TEMPERATURE_DEGC   40
  
//Variables/flags:
// Error flags
static bool mcuUndervoltage_b = false;      // EN Vsup < 2.90V DIS Vsup > 2.95V 
static bool pirUndervoltage_b = false;      // EN Vbat < 3.78V DIS Vbat > 3.80V 
static bool batteryUndervoltage_b = false;  // EN Vbat < 3.60V DIS Vbat > 3.65V
static bool batteryOvervoltage_b = false;   // EN Vbat < 4.20V DIS Vbat > 4.15V
static bool lowTemperature_b = false;       // EN temp < 5degC DIS temp > 8degC
static bool highTemperature_b = false;      // EN temp > 45degC DIS temp > 40degC

static StateOfCharge chargingState = DISABLED;

static void setFlag(int value, int enable, int disable, bool& currentFlag);

//Error check functions:
void batteryOvervoltageCheck();
void batteryUndervoltageCheck();
void mcuUndervoltageCheck();
void pirUndervoltageCheck();
void lowTemperatureCheck();
void highTemperatureCheck();

StateOfCharge getChargingState();
bool batteryOvervoltage();
bool batteryUndervoltage();
bool mcuUndervoltage();
bool pirUndervoltage();
bool lowTemperature();
bool highTemperature();

//Main control functions:
void chargeManager(); // sets chargingActive and solarDisconnected
void errorControl();

//Getters/setters:
bool isNight();
bool isCharging();
// Initializing function
void initPowerManager();

#endif
