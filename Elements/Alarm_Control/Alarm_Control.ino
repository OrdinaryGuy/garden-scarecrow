#include "ALRM.h"

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  analogReference(INTERNAL);
  pinMode(EXT_GPIO_SW1_PIN, INPUT);
  initAlarmControl();
}

void loop() {
  // put your main code here, to run repeatedly:
  static bool swState = false;
  if(digitalRead(EXT_GPIO_SW1_PIN) != swState){
    swState = !swState;
    buzzVoltage(analogRead(BATT_SENS_PIN)*6);
    delay(2000);
    scare();
  }
}
