#ifndef AM_H
#define AM_H

#include <Arduino.h>

#ifndef CONFIG_H
#define CONFIG_H
#define INLINING_EN

/* Additional functionality port */
#define EXT_GPIO_AD_PIN   A3
#define EXT_GPIO_D_PIN    3
#define EXT_NFET_SW_PIN   9

/* External switch/connector */
#define EXT_GPIO_SW1_PIN  2
#define EXT_GPIO_SW2_PIN  4

/* Digital outputs */
#define BOOST_SW_PIN    A2
#define TEMP_SW_PIN     A1
#define SOLAR_SW_PIN    13
#define PIR_SW_PIN      11
#define BUZZER_SW_PIN   10

/* Digital input */
#define PIR_SENS_PIN    12

/* Analog inputs */
#define TEMP_SENS_PIN   A0
#define SOLAR_SENS_PIN  A7
#define BATT_SENS_PIN   A6

#endif

#define V_REF_mV        (int16_t)1078
#define DIVIDER_FACTOR  (int16_t)6
#define ADC_MAX_VAL     (int16_t)1023

// Converted analog values
static int16_t solarVoltage;
static int16_t batteryVoltage;
static int16_t supplyVoltage;
static int8_t  temperature;
static float analogValue;
// Conversion factor for analogValue calculation
static float conversionFactor = 0.0;

// Static internal functions
static int16_t readVcc();
static int16_t readVbat();
static int16_t readVsol();
static int8_t readTemp();
static float readAnalogValue();

// Initialization function
void initAnalogMeasurements();

void updateAnalogValues();

#ifdef INLINING_EN
inline int16_t getVcc(){return supplyVoltage;}
inline int16_t getVbat(){return batteryVoltage;}
inline int16_t getVsol(){return solarVoltage;}
inline float getAnalogValue(){return analogValue;}
inline int8_t getTemperature(){return temperature;}

inline float getConversionFactor(){return conversionFactor;}
inline void setConversionFactor(float convFactor){conversionFactor = convFactor;}

#else
int16_t getVcc();
int16_t getVbat();
int16_t getVsol();
float getAnalogValue();
int8_t getTemperature();

float getConversionFactor();
void setConversionFactor(float convFactor);
#endif
#endif
