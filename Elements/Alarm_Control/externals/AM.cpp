#include "AM.h"

#ifndef INLINING_EN
int16_t getVcc(){return supplyVoltage;}
int16_t getVbat(){return batteryVoltage;}
int16_t getVsol(){return solarVoltage;}
float getAnalogValue(){return analogValue;}
int8_t getTemperature(){return temperature;}

float getConversionFactor(){return conversionFactor;}
void setConversionFactor(float convFactor){conversionFactor = convFactor;}
#endif
/**
 * @brief Constructor sets NTC power pin, reference and first dataset
 */
void initAnalogMeasurements()
{
  analogReference(INTERNAL);              // From default to internal it takes around 6ms for reference to settle
  int dummy = analogRead(BATT_SENS_PIN);  // Only analogRead function sets voltage reference
  delay(6);                               // Requirement for internal reference to settle
  pinMode(TEMP_SW_PIN, OUTPUT);
  updateAnalogValues();                   // To have initial values for processing
}

/**
 * @brief Function measures all ADC values of the board. Requires at least 6ms delays between calls to let reference voltages to settle properly
 */
void updateAnalogValues() { // Around 1.8ms to complete
  ADCSRA |= (1 << ADEN);                  // Enable ADC

  for(byte i = 0; i < 4; i++)             // Discard first few measurements
    batteryVoltage = readVbat();
  solarVoltage = readVsol();
  analogValue = readAnalogValue();
  
  analogReference(DEFAULT);               // From internal to default it takes around 30us to settle
  digitalWrite(TEMP_SW_PIN, HIGH);
  for(byte i = 0; i < 4; i++)             // Switching reference voltage and powering NTC may result in fist few measurements to be incorrect. So discard few of them
    temperature = readTemp();
  digitalWrite(TEMP_SW_PIN, LOW);
  supplyVoltage = readVcc();              // Measuring supply voltage against internal reference

  analogReference(INTERNAL);              // From default to internal it takes around 6ms for reference to settle
  int dummy = analogRead(BATT_SENS_PIN);  // Only analogRead function sets voltage reference
  
  ADCSRA &= ~(1 << ADEN);                 // Disable ADC
}

static int16_t readVcc() {
  int32_t result;
  // Read 1.1V reference against AVcc
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1); // Supply voltage
  //delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Conversion begin
  while (bit_is_set(ADCSRA,ADSC));
  result = ADCL;
  result |= ADCH<<8;
  result = ((int32_t)ADC_MAX_VAL * V_REF_mV) / result; // Back-calculate AVcc in mV
  return (int16_t)result;
}

static int16_t readVbat() {
  int32_t result = analogRead(BATT_SENS_PIN);
  return (int16_t)((int32_t)(V_REF_mV * DIVIDER_FACTOR) * result / ADC_MAX_VAL);
}

static int16_t readVsol() {
  int32_t result = analogRead(SOLAR_SENS_PIN);
  return (int16_t)((int32_t)(V_REF_mV * DIVIDER_FACTOR) * result / ADC_MAX_VAL);
}

static float readAnalogValue() {
  if(conversionFactor != 0.0)
  {
    int16_t adcVal = analogRead(EXT_GPIO_AD_PIN);
    return (float)adcVal * (float)V_REF_mV * conversionFactor / (float)ADC_MAX_VAL;
  }
  else
  {
    return 0.0;
  }
}

static int8_t readTemp() {

  int8_t tempTresholds[] = {-20, -10,   0,  10,  20,  30,  40,  60,  80, 100};
  int16_t adcTresholds[] = {923, 859, 775, 675, 566, 460, 364, 219, 130, 78};

  uint8_t arrLength = sizeof(tempTresholds)/sizeof(tempTresholds[0]);

  int16_t temp = analogRead(TEMP_SENS_PIN);

  if(adcTresholds[0] < temp){
    return -21;
  }
  else if(adcTresholds[arrLength -1] > temp){
    return 101;
  }
  else{
    uint8_t idx = 1;
    while(adcTresholds[idx] >= temp){idx++;}
    return (uint8_t)map(temp, adcTresholds[idx - 1], adcTresholds[idx], tempTresholds[idx - 1], tempTresholds[idx]);
  }
}
