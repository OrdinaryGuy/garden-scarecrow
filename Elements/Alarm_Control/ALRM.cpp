#include "ALRM.h"

void initAlarmControl(){
  pinMode(BOOST_SW_PIN, OUTPUT);
  pinMode(BUZZER_SW_PIN, OUTPUT);

  digitalWrite(BOOST_SW_PIN, LOW);
  digitalWrite(BUZZER_SW_PIN, LOW);  
}

static uint16_t randomNumberLimit(float base ,float offset, float currentScareTime_ms, float scareTime_ms){
  return (uint16_t)((scareTime_ms * (1.0 - pow(base,currentScareTime_ms/scareTime_ms))) + offset);
}

void scare(scareRange duration){
  uint32_t timestamp = millis();
  uint32_t scareTime = 200;
  uint32_t buzz = timestamp;
  uint32_t led = timestamp;
  randomSeed(timestamp);
  
  // Scare duration calculation
  uint16_t scareDuration = 5000;
  switch(duration){
    case SHORT_SCARE: scareDuration = random(900,2500); break;
    case MEDIUM_SCARE: scareDuration = random(1500,4500); break;
    case LONG_SCARE: scareDuration = random(3500,7000); break;
    default: break;
  }

  uint16_t Rmin;
  uint16_t Rmax;

  // Initial scare sequence
  togglePins(20, 20, 5);
  // Main random scare sequence
  while(scareTime < scareDuration){
    if(millis() > buzz){
      scareTime = millis() - timestamp;
      Rmin = randomNumberLimit(0.9, 0.02, (float)scareTime, (float)scareDuration);
      Rmax = randomNumberLimit(0.85, 0.1, (float)scareTime, (float)scareDuration);
      buzz = millis() + random(Rmin, Rmax);
      digitalWrite(BUZZER_SW_PIN, !digitalRead(BUZZER_SW_PIN));
    }
    if(millis() > led){
      scareTime = millis() - timestamp;
      Rmin = randomNumberLimit(0.9, 0.02, (float)scareTime, (float)scareDuration);
      Rmax = randomNumberLimit(0.85, 0.1, (float)scareTime, (float)scareDuration);
      led = millis() + random(Rmin, Rmax);
      digitalWrite(BOOST_SW_PIN, !digitalRead(BOOST_SW_PIN));
    }
  }
  // Terminal scare sequence
  togglePins(scareDuration/4, 1, 1);
}

void buzzVoltage(int voltage_mV){
  int16_t volts = voltage_mV / 1000;
  int16_t tenths = (voltage_mV % 1000) / 100;
  togglePins(250, 80, volts, BUZZER_SW_PIN);
  delay(500);
  togglePins(80, 250, tenths, BUZZER_SW_PIN);
}

void togglePins(uint16_t highDuration_ms, uint16_t lowDuration_ms, uint8_t repeats, uint8_t pin){
  for(uint8_t i = 0; i < repeats; i++){
    if(pin > 18){
      digitalWrite(BOOST_SW_PIN, HIGH);
      digitalWrite(BUZZER_SW_PIN, HIGH);
    }
    else{
      digitalWrite(pin, HIGH);
    }
    delay(highDuration_ms);

    if(pin > 16){
      digitalWrite(BOOST_SW_PIN, LOW);
      digitalWrite(BUZZER_SW_PIN, LOW);
    }
    else{
      digitalWrite(pin, LOW);
    }
    delay(lowDuration_ms);
  }
}
