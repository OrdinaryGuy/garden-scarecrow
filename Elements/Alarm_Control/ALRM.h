#ifndef ALRM_H
#define ALRM_H

#include <Arduino.h>
#include "externals\config.h"

enum scareRange : uint8_t
{
  SHORT_SCARE,
  MEDIUM_SCARE,
  LONG_SCARE
};

void initAlarmControl();

static uint16_t randomNumberLimit(float base ,float offset, float currentScareTime_ms, float scareTime_ms);

void scare(scareRange duration = MEDIUM_SCARE);
void togglePins(uint16_t highDuration_ms, uint16_t lowDuration_ms, uint8_t repeats, uint8_t pin = 255);
void buzzVoltage(int voltage_mV);

#endif
