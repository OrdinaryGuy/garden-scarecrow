#include <AM.h>

void setup() {
  Serial.begin(9600);
  delay(5000);
  initAnalogMeasurements();
  setConversionFactor(1.0);

  // Enable WDG to wake up every second and run ISR
  WDTCSR = 24;
  WDTCSR = 6;
  WDTCSR |= (1 << 6);

  // Enable deep sleep mode
  SMCR |= (1 << 2);
}

void loop() {
  sprint("Vcc", getVcc());
  sprint("Vbat", getVbat());
  sprint("Vsol", getVsol());
  sprint("Temp", getTemperature());
  sprint("ADC", (int)getAnalogValue());
  Serial.println();
  delay(100);

  SMCR |= 1;                      // Enable sleep mode before going to sleep
  __asm__ __volatile__("sleep");  // In line assembler to go to sleep
  SMCR &= ~1;                     // Disable sleep after wake up
}

ISR(WDT_vect) {
  updateAnalogValues();
}

void sprint(String s, int val){
  Serial.print(s);
  Serial.print(": ");
  Serial.println(val);
}
