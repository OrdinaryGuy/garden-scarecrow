#ifndef AM_H
#define AM_H

#include <Arduino.h>

#ifndef CONFIG_H
#define CONFIG_H

/* Additional functionality port */
#define EXT_GPIO_AD_PIN   A3
#define EXT_GPIO_D_PIN    3
#define EXT_NFET_SW_PIN   9

/* External switch/connector */
#define EXT_GPIO_SW1_PIN  2
#define EXT_GPIO_SW2_PIN  4

/* Digital outputs */
#define BOOST_SW_PIN    A2
#define TEMP_SW_PIN     A1
#define SOLAR_SW_PIN    13
#define PIR_SW_PIN      11
#define BUZZER_SW_PIN   10

/* Digital input */
#define PIR_SENS_PIN    12

/* Analog inputs */
#define TEMP_SENS_PIN   A0
#define SOLAR_SENS_PIN  A7
#define BATT_SENS_PIN   A6

#endif

#define V_REF_mV        (int16_t)1083	// Measured internal reference
#define DIVIDER_FACTOR  (int16_t)6	// Voltage dividers on solar and battery sides 1 + (Rup/Rdown)
#define ADC_RESOLUTION  (int16_t)1024

// Converted analog values in mV and degC
static int16_t solarVoltage;
static int16_t batteryVoltage;
static int16_t supplyVoltage;
static int8_t  temperature;
// Extra analog value for another purposes with conversion factor for analogValue calculation
static float analogValue;
static float conversionFactor = 0.0;	// This extra conversion is performed only when conversion factor is != 0

// Static internal functions
static int16_t readVcc();
static int16_t readVbat();
static int16_t readVsol();
static int8_t readTemp();
static float readAnalogValue();

// Public functions
void initAnalogMeasurements(); 	// Initial initialization function
void updateAnalogValues();	// Call for fresh values

// Getters and setters
int16_t getVcc();
int16_t getVbat();
int16_t getVsol();
float getAnalogValue();
int8_t getTemperature();

float getConversionFactor();
void setConversionFactor(float convFactor);

#endif
