#include "PWRM.h"

/** 
 * Function sets/resets anz boolean flag based on current integer variable and given boundary values
 */
static void setFlag(int value, int enable, int disable, bool& currentFlag){
  if(enable < disable){
    if((enable > value) && (currentFlag == false)){ currentFlag = true; return;}
    if((disable < value) && (currentFlag == true)){ currentFlag = false;}
  }
  else if(enable > disable){
    if((value > enable) && (currentFlag == false)){ currentFlag = true; return;}
    if((value < disable) && (currentFlag == true)){ currentFlag = false;}
  }
}

/**
 * Measured value checks
 */
void batteryOvervoltageCheck(){
  setFlag(getVbat(), SET_BATT_OVERVOLTAGE_MV, RESET_BATT_OVERVOLTAGE_MV, batteryOvervoltage_b);
}
void batteryUndervoltageCheck(){
  setFlag(getVbat(), SET_BATT_UNDERVOLTAGE_MV, RESET_BATT_UNDERVOLTAGE_MV, batteryUndervoltage_b);
}
void mcuUndervoltageCheck(){
  setFlag(getVcc(), SET_MCU_UNDERVOLTAGE_MV, RESET_MCU_UNDERVOLTAGE_MV, mcuUndervoltage_b);
}
void pirUndervoltageCheck(){
  setFlag(getVbat(), SET_PIR_UNDERVOLTAGE_MV, RESET_PIR_UNDERVOLTAGE_MV, pirUndervoltage_b);
}
void lowTemperatureCheck(){
  setFlag(getTemperature(), SET_LOW_TEMPERATURE_DEGC, RESET_LOW_TEMPERATURE_DEGC, lowTemperature_b);
}
void highTemperatureCheck(){
  setFlag(getTemperature(), SET_HIGH_TEMPERATURE_DEGC, RESET_HIGH_TEMPERATURE_DEGC, highTemperature_b);
}

/**
 * Function controls current flowing to battery from solar panel. Expect to run in one minute intervals
 */
void chargeManager(){
  static byte waitCycles = 0;
  // Vbat is expected to swing between RESET_CHARGING_VOLTAGE_MV and SET_CHARGING_VOLTAGE_MV which is done here
  static bool expected_charging = true;
  setFlag(getVbat(), SET_CHARGING_VOLTAGE_MV, RESET_CHARGING_VOLTAGE_MV, expected_charging);

  // If charging expected, check safe temperature range for battery
  if(expected_charging && (!lowTemperature_b && !highTemperature_b)){
    switch(chargingState){
      case DISABLED:    if(getVsol() > SET_SOLAR_VOLTAGE_MV){ // Check solar cell to be tested
                          digitalWrite(SOLAR_SW_PIN, HIGH);
                          chargingState = UNDER_TEST;
                        }
                        break;
      case UNDER_TEST:  if((getVsol() - getVbat()) >= MIN_DELTA_SOL_BATT_VOLTAGE_MV){ // Sufficient current crudely measured accross diode on solar panel
                          chargingState = CHARGING;
                        }
                        else{
                          digitalWrite(SOLAR_SW_PIN, LOW);
                          chargingState = TEST_FAILED;
                          waitCycles = DELAY_BETWEEN_SOLAR_TESTS_MIN;
                        }
                        break;
      case CHARGING:    if((getVsol() - getVbat()) < MIN_DELTA_SOL_BATT_VOLTAGE_MV){ // Diode voltage drop is used as crude current sensor
                          digitalWrite(SOLAR_SW_PIN, LOW);
                          chargingState = DISABLED;
                        }
                        break;
      case TEST_FAILED:  if(--waitCycles == 0) chargingState = DISABLED; // Count down minutes before next try
                        break;
      default:          chargingState = DISABLED; // Returning to default state
                        break;
    }
  }
  else{ // Error occured or battery is full
    digitalWrite(SOLAR_SW_PIN, LOW);
    chargingState = DISABLED;
  }
}

StateOfCharge getChargingState(){
  return chargingState;
}

bool batteryOvervoltage(){
  return batteryOvervoltage_b;
}
bool batteryUndervoltage(){
  return batteryUndervoltage_b;
}
bool mcuUndervoltage(){
  return mcuUndervoltage_b;
}
bool pirUndervoltage(){
  return pirUndervoltage_b;
}
bool lowTemperature(){
  return lowTemperature_b;
}
bool highTemperature(){
  return highTemperature_b;
}

void errorControl(){
  // Battery overvoltage
  setFlag(getVbat(), SET_BATT_OVERVOLTAGE_MV, RESET_BATT_OVERVOLTAGE_MV, batteryOvervoltage_b);
  // Battery undervoltage
  setFlag(getVbat(), SET_BATT_UNDERVOLTAGE_MV, RESET_BATT_UNDERVOLTAGE_MV, batteryUndervoltage_b);
  // Vcc gets below certain voltage. Below this limit, brownout is possible
  setFlag(getVcc(), SET_MCU_UNDERVOLTAGE_MV, RESET_MCU_UNDERVOLTAGE_MV, mcuUndervoltage_b);
  // Unsufficient battery voltage for PIR sensor check
  setFlag(getVbat(), SET_PIR_UNDERVOLTAGE_MV, RESET_PIR_UNDERVOLTAGE_MV, pirUndervoltage_b);
  // Low temperature check
  setFlag((int16_t)getTemperature(), SET_LOW_TEMPERATURE_DEGC, RESET_LOW_TEMPERATURE_DEGC, lowTemperature_b);
  // High temperature check
  setFlag((int16_t)getTemperature(), SET_HIGH_TEMPERATURE_DEGC, RESET_HIGH_TEMPERATURE_DEGC, highTemperature_b);
}

bool isNight(){
  static bool isNight = false;
  setFlag(getVsol(), SET_NIGHT_REGIME_VOLTAGE_MV, RESET_NIGHT_REGIME_VOLTAGE_MV, isNight);
  return isNight;
}

bool isCharging(){
  if(chargingState == CHARGING) return true;
  return false;
}
 
void initPowerManager(){
  pinMode(SOLAR_SW_PIN, OUTPUT);
  digitalWrite(SOLAR_SW_PIN, LOW);
}
