#include "config.h"

#include "AM.h"
#include "PWRM.h"
#include "ALRM.h"
#include "SCRM.h"

#include <EEPROM.h>
#include <Wire.h>

volatile uint32_t secCnt = 0;
volatile uint8_t minAction = 1;
bool isActive = false; 
bool pirSensorFalut = false;
bool switchState = false;
uint16_t dataTimestamp = 30;

#define MIN_ACTION_INTERVAL   60

void setup() {
  Serial.begin(115200);
  Wire.begin();
  initAnalogMeasurements();
  initPowerManager();
  initAlarmControl();
  // Switches
  pinMode(EXT_GPIO_SW1_PIN, INPUT);
  pinMode(EXT_GPIO_SW2_PIN, INPUT);
  // Extra pins
  pinMode(EXT_GPIO_AD_PIN, INPUT_PULLUP);
  pinMode(EXT_GPIO_D_PIN, INPUT_PULLUP);
  pinMode(EXT_NFET_SW_PIN, INPUT_PULLUP);
  // Unconnected pins
  pinMode(5, OUTPUT);
  pinMode(6, INPUT_PULLUP);
  pinMode(7, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
  // I2C
  pinMode(A4, INPUT_PULLUP);
  pinMode(A5, INPUT_PULLUP);

  readEEPROM();

//  Wire.beginTransmission(0x50);
//  Wire.write(0);
//  Wire.write(3);
//  Wire.write(0xA5);
//  Wire.write(0xFF);
//  Wire.endTransmission();

  delay(5);

  Wire.beginTransmission(0x50);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();

  Wire.requestFrom(0x50, 32);
  while(Wire.available())
    Serial.println(Wire.read());

  // Initial buzz
  togglePins(20, 80, 5, BUZZER_SW_PIN);
  delay(1500);
  togglePins(600, 1, 1, BUZZER_SW_PIN);
  
  // Enable WDG to wake up every second and run ISR
  WDTCSR = 24;
  WDTCSR = 6;
  WDTCSR |= (1 << 6);

  // Enable deep sleep mode
  SMCR |= (1 << 2);

}

void loop() {
  if(minAction){
    minAction--;
    
    // Order is important -> Get values -> check errors -> do other stuff
    updateAnalogValues();
    errorControl();
    chargeManager();
    // repeat action next second when solar system is under test
    if(getChargingState() == UNDER_TEST)
      minAction++;

    //Serial.print((String) "Temp: " + getTemperature() + "\tVbat: " +  getVbat() + "\tVsol: " + getVsol() + "\tVcc: " + getVcc() + "\n");
    //delay(10);
  }

  //if night and conditions are favourable be prepared for scaring
  scareManager_1sTask();
  usrInteraction();

  if(switchState)
    storageManager();

  digitalWrite(5, LOW);
  
  // Back to sleep
  SMCR |= 1;                      // Enable sleep mode before going to sleep
  __asm__ __volatile__("sleep");  // In line assembler to go to sleep
  SMCR &= ~1;                     // Disable sleep after wake up

  digitalWrite(5, HIGH);
}

ISR(WDT_vect) {
  if(++secCnt % MIN_ACTION_INTERVAL == 0){
    minAction++;
  }
}

/**
 * Function shifts active regime defined delay into night time and defined delay into daytime. Has to run in 1s intervals
 */
 /*
bool activeRegime(bool nightTime){
  if(nightTime){ // dark
    if(nightRegime == false){
      if(++regimeCnt >= TO_ACTIVE_DELAY_SEC){
        nightRegime = true;
        regimeCnt = 0;
      }
    }
    else if(regimeCnt > 0){
      regimeCnt--;
    }
  }
  else{ // light
    if(nightRegime ){
      if(++regimeCnt >= TO_PASSIVE_DELAY_SEC){
        nightRegime = false;
        regimeCnt = 0;
      }
    }
    else if(regimeCnt > 0)
      regimeCnt--;
  }
  isActive = nightRegime;
  return nightRegime;
}

#define PIR_START_UP_DLY_SEC            60
#define TIMER_SCARE_INTERVAL_SEC        600
#define TIMER_SCARE_INTERVAL_NO_PIR_SEC 420
#define MIN_PIR_SCARE_INTERVAL_SEC      1000
#define PIR_FAULT_DETECTED_INTERVAL_SEC 3600
#define PIR_IGNORE_INTERVAL_SEC         10
void scareManager(){
  static uint16_t scareCounter = 0;
  static uint16_t scareTimer = 0;
  static uint8_t pirScareDelay = 0;
  static uint8_t pirIgnoreCnt = 0;
  static bool pir_fault = false;
  
  // device is active only when regime is active as well as battery and mcu voltage is sufficient
  if(activeRegime(isNight()) && !batteryUndervoltage() && !mcuUndervoltage()){

    // PIR voltage unsufficient for operation so only timer scaring
    if(pirUndervoltage()){
      // Power down PIR sensor if active
      if(digitalRead(PIR_SW_PIN)) 
        digitalWrite(PIR_SW_PIN, LOW);

      // Scare timing
      if(++scareTimer >= TIMER_SCARE_INTERVAL_NO_PIR_SEC){
        scare(SHORT_SCARE);
        scareTimer = 0;
      }
    // timer scaring and sensor scaring 
    }else{
      // PIR sensor start up, sensor must be on and ignore cnt greater than 0
      if(digitalRead(PIR_SW_PIN)){
        if(pirIgnoreCnt)
          pirIgnoreCnt--;
      }else{
        digitalWrite(PIR_SW_PIN, HIGH);
        pirIgnoreCnt = PIR_START_UP_DLY_SEC;
      }

      if(!pirIgnoreCnt){
        if(!pir_fault && digitalRead(PIR_SENS_PIN)){
          scare(LONG_SCARE);
          scareTimer = 0;
          pirIgnoreCnt += PIR_IGNORE_INTERVAL_SEC; // delay for pir sensor to relax useful for weak batteries voltage fluctuates more with load
          
          // PIR fault detection based on too frequent sensored scaring
          scareCounter += MIN_PIR_SCARE_INTERVAL_SEC;
          if(scareCounter > PIR_FAULT_DETECTED_INTERVAL_SEC){
            pir_fault = true;
          }
            
        }else if(scareTimer >= TIMER_SCARE_INTERVAL_SEC){
          scare();
          scareTimer = 0;
          pirIgnoreCnt += PIR_IGNORE_INTERVAL_SEC; // delay for pir sensor to relax
        }
      }
      scareTimer++;

      // decrementing scareCnt and reset pir_fault flag
      if(scareCounter) scareCounter--;
      else pir_fault = false;
      // error logging
      pirSensorFalut = pir_fault;
    }

  } else { // PIR turn off
    digitalWrite(PIR_SW_PIN, LOW);
    scareCounter = 0;
    scareTimer = 0;
    pirScareDelay = 0;
    pirIgnoreCnt = 0;
    pir_fault = false;
  }
}*/

void usrInteraction(){
  static bool prev_state = digitalRead(EXT_GPIO_SW2_PIN);
  switchState = digitalRead(EXT_GPIO_SW2_PIN);
  if(!mcuUndervoltage() && switchState != prev_state){
    prev_state = switchState;
    buzzVoltage(getVbat());
    
    if(switchState == true)
    {
      togglePins(20, 580, 5, BOOST_SW_PIN);
      dataTimestamp = (uint16_t)(secCnt/60) + 30;
    }
    else
    {
      delay(500);
      togglePins(300, 10, 1, BUZZER_SW_PIN);
    }
  }

  
}
/*
void storageManager(void){
  static uint16_t address = 0;

  static uint8_t prev_bitField = 0;
  uint8_t bitField = 0;
  bitField |= pirSensorFalut << 0;
  bitField |= batteryUndervoltage() << 1;
  bitField |= mcuUndervoltage() << 2;
  bitField |= pirUndervoltage() << 3;
  bitField |= lowTemperature() << 4;
  bitField |= highTemperature() << 5;
  bitField |= isActive << 6;
  bitField |= isCharging() << 7;

  if(bitField != prev_bitField){
    prev_bitField = bitField;
    if(address < 1000){
      uint16_t minPassed = (uint16_t)(secCnt/60);
      EEPROM.put(address,minPassed);
      EEPROM.put(address + 2, getVbat());
      EEPROM.put(address + 4, getVsol());
      EEPROM.put(address + 6, getVcc());
      EEPROM.put(address + 8, getTemperature());
      EEPROM.put(address + 9, bitField);
      address += 10;
    }
    delay(10);
  }
}
*/

void storageManager(void){
  static uint16_t error_add = 840;
  static uint16_t data_add = 0;

  static uint8_t prev_bitField = 0;
  uint8_t bitField = 0;
  bitField |= (byte)pirSensorFalut << 0;
  bitField |= (byte)batteryUndervoltage() << 1;
  bitField |= (byte)mcuUndervoltage() << 2;
  bitField |= pirUndervoltage() << 3;
  bitField |= lowTemperature() << 4;
  bitField |= highTemperature() << 5;
  bitField |= isActive << 6; /* Not an error */
  // Add scaring triggered bit
  //bitField |= isCharging() << 7; /* Proven to work as planned */

  uint16_t minPassed = (uint16_t)(secCnt/60);
  
  if(bitField != prev_bitField){
    prev_bitField = bitField;
    if(error_add <= 1020){
      
      EEPROM.put(error_add,minPassed);
      EEPROM.put(error_add + 2, bitField);
      error_add += 3;
      
      delay(10);
    }
  }

  if(minPassed >= dataTimestamp)
  {
    dataTimestamp += 30;
    if(data_add < 840){
      EEPROM.put(data_add + 0, getVbat());
      EEPROM.put(data_add + 2, getVsol());
      EEPROM.put(data_add + 4, getVcc());
      EEPROM.put(data_add + 6, getTemperature());
      data_add += 7;

      delay(10);
    }
  }
}

/*
void readEEPROM(){
  uint16_t add = 0;
  uint16_t timeInMinutes = 0;
  int16_t voltage = 0;
  uint8_t data = 0;

  for( add = 0; add <= (EEPROM.length() - 10); add += 10){
    data = EEPROM.read(add+1);
    if(data != 255){
      Serial.print((String) (add / 10) + "\t");
      EEPROM.get(add, timeInMinutes);
      Serial.print((String)"Time(min): " + timeInMinutes + "\t");
      EEPROM.get(add + 2, voltage);
      Serial.print((String)"Vbat(mV): " + voltage + "\t");
      EEPROM.get(add + 4, voltage);
      Serial.print((String)"Vsol(mV): " + voltage + "\t");
      EEPROM.get(add + 6, voltage);
      Serial.print((String)"Vcc(mV): " + voltage + "\t");
      Serial.print((String)"Temperature: " + EEPROM.read(add+8) + "\t");
      data = EEPROM.read(add+9);
    
      if (bitRead(data, 0) == 1) {
        Serial.print("PIRflt");
        Serial.print("\t");
      }
      if (bitRead(data, 1) == 1) {
        Serial.print("battUV");
        Serial.print("\t");
      }
      if (bitRead(data, 2) == 1) {
        Serial.print("mcuUV");
        Serial.print("\t");
      }
      if (bitRead(data, 3) == 1) {
        Serial.print("pirUV");
        Serial.print("\t");
      }
      if (bitRead(data, 4) == 1) {
        Serial.print("lowTemp");
        Serial.print("\t");
      }
      if (bitRead(data, 5) == 1) {
        Serial.print("highTemp");
        Serial.print("\t");
      }
      if (bitRead(data, 6) == 1) {
        Serial.print("active");
        Serial.print("\t");
      }
      if (bitRead(data, 7) == 1) {
        Serial.print("CHRG");
        Serial.print("\t");
      }
      Serial.println("");
    } else {
      break;
    }
  }
}
*/

void readEEPROM(){
  uint16_t add = 0;
  uint16_t timeInMinutes = 0;
  int16_t voltage = 0;
  uint8_t data = 0;

  Serial.println("Time(min)\tVbat\tVsol\tVcc\tTemp");

  for( add = 0; add < 840; add += 7){
    data = EEPROM.read(add+6);
    if(data != 255){
      uint16_t minPassed = (add/7 + 1)* 30;
      Serial.print((String) minPassed + "\t");
      EEPROM.get(add, voltage);
      Serial.print((String) voltage + "\t");
      EEPROM.get(add + 2, voltage);
      Serial.print((String) voltage + "\t");
      EEPROM.get(add + 4, voltage);
      Serial.print((String) voltage + "\t");
      Serial.print((String) data + "\t\n");
    }
    else{
      break;
    }
  }

  Serial.println();

  for( add = 840; add <= 1020; add += 3){
    
    EEPROM.get(add, timeInMinutes);
    Serial.print((String)"Time(min): " + timeInMinutes + "\t");
    data = EEPROM.read(add+2);
    
    if (bitRead(data, 0) == 1) {
      Serial.print("PIRflt");
      Serial.print("\t");
    }
    if (bitRead(data, 1) == 1) {
      Serial.print("battUV");
      Serial.print("\t");
    }
    if (bitRead(data, 2) == 1) {
      Serial.print("mcuUV");
      Serial.print("\t");
    }
    if (bitRead(data, 3) == 1) {
      Serial.print("pirUV");
      Serial.print("\t");
    }
    if (bitRead(data, 4) == 1) {
      Serial.print("lowTemp");
      Serial.print("\t");
    }
    if (bitRead(data, 5) == 1) {
      Serial.print("highTemp");
      Serial.print("\t");
    }
    if (bitRead(data, 6) == 1) {
      Serial.print("active");
      Serial.print("\t");
    }
    if (bitRead(data, 7) == 1) {
      Serial.print("CHRG");
      Serial.print("\t");
    }
    Serial.println("");
  }
}
