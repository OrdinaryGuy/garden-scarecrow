#include "SCRM.h"

/* Function pointer for accessing state machine functions */
typedef void(*p_stateFunction)(uint16_t * wait);

static p_stateFunction p_mainAppFunction = state_IDLE;
static bool pir_fault = false;
static bool nightRegime = false;
static running_mode MODE_e = TIMER_ONLY;

/**
 * Setter for mode selection
 */
void setScareMode(running_mode mode){
  MODE_e = mode;
}

/**
 * Function shifts active regime defined delay into night time and defined delay into daytime. Has to run in defined intervals
 */
void activeRegime(bool nightTime){
  static uint16_t regimeCnt = 0;
  
  if(nightRegime != nightTime){
    regimeCnt++;
    if((nightRegime && regimeCnt >= TO_PASSIVE_DELAY_SEC) || (!nightRegime && regimeCnt >= TO_ACTIVE_DELAY_SEC)){
      nightRegime = !nightRegime;
      regimeCnt = 0;
    }
  }
  else if(regimeCnt)
    regimeCnt--;
}

/**
 * Checking operation related faults to enter active regime
 */
static bool system_OK(void){
  if(!lowTemperature() && !highTemperature() && !batteryUndervoltage() && !mcuUndervoltage())
    return true;
  else
    return false;
}

static void state_IDLE(uint16_t * wait){
  // device is active only when regime is active as well as battery and mcu voltage is sufficient
  if(nightRegime && system_OK()){
    switch(MODE_e){
    case NO_OPERATION:    break;
    case TIMER_ONLY:      p_mainAppFunction = state_TIMER_ONLY;
                          break;
    case SENSOR_ONLY:     p_mainAppFunction = state_SENSOR_ONLY;
                          break;
    case TIMER_AND_SENSOR:p_mainAppFunction = state_TIMER_AND_SENSOR;
                          break;
    default:  MODE_e == NO_OPERATION;
              break;
    }
  }
  else{
    digitalWrite(PIR_SW_PIN, LOW);
    *wait = 0;
    /* TODO: reset PIR fault*/
  }
}

static void state_TIMER_ONLY(uint16_t * wait){
  if(!nightRegime || !system_OK()){
    p_mainAppFunction = state_IDLE;
    return;
  }
  scare(MEDIUM_SCARE);
  *wait = TIMER_SCARE_INTERVAL_NO_PIR_SEC;
}

static void state_SENSOR_ONLY(uint16_t * wait){
  static uint16_t scareCnt = 0;
  if(!nightRegime || !system_OK()){
    p_mainAppFunction = state_IDLE;
    scareCnt = 0;
  }
  else if(!digitalRead(PIR_SW_PIN) && !pir_fault){
    digitalWrite(PIR_SW_PIN, HIGH);
    *wait = PIR_START_UP_DLY_SEC;
  }
  else if (!pir_fault && digitalRead(PIR_SENS_PIN)){
    scare(LONG_SCARE);
    *wait = PIR_IGNORE_INTERVAL_SEC; // delay for pir sensor to relax useful for weak batteries voltage fluctuates more with load
          
    // PIR fault detection based on too frequent sensored scaring
    scareCnt += MIN_PIR_SCARE_INTERVAL_SEC;
    if(scareCnt > PIR_FAULT_DETECTED_INTERVAL_SEC){
      pir_fault = true;
    }
  }

  // Pir sensor fault autoreset
  if(scareCnt){
    scareCnt--;
  }
  else if(pir_fault){
    pir_fault = false;
  }
}

static void state_TIMER_AND_SENSOR(uint16_t * wait){
  // TODO: For the future
}

void scareManager_1sTask(void){
  // variable holding number of skipped cycles
  static uint16_t wait_cycles = 0;

  // function deciding whether device is active or not
  activeRegime(isNight());
  
  // function pointer runs only when wait_cycles == 0
  if(!wait_cycles)
    p_mainAppFunction(&wait_cycles);
  
  // decrement counter each execution down to 0
  if(wait_cycles)
    wait_cycles--;
}
