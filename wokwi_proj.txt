/* Additional functionality port */
#define EXT_GPIO_AD_PIN   A3
#define EXT_GPIO_D_PIN    3
#define EXT_NFET_SW_PIN   9

/* External switch/connector */
#define EXT_GPIO_SW1_PIN  2
#define EXT_GPIO_SW2_PIN  4

/* Digital outputs */
#define BOOST_SW_PIN    A2
#define TEMP_SW_PIN     A1
#define SOLAR_SW_PIN    13
#define PIR_SW_PIN      11
#define BUZZER_SW_PIN   10

/* Digital input */
#define PIR_SENS_PIN    12

/* Analog inputs */
#define TEMP_SENS_PIN   A0
#define SOLAR_SENS_PIN  A7
#define BATT_SENS_PIN   A6

#define TO_ACTIVE_DELAY_SEC   36
#define TO_PASSIVE_DELAY_SEC  36

#define PIR_START_UP_DLY_SEC            2
#define TIMER_SCARE_INTERVAL_SEC        6
#define TIMER_SCARE_INTERVAL_NO_PIR_SEC 4
#define MIN_PIR_SCARE_INTERVAL_SEC      10
#define PIR_FAULT_DETECTED_INTERVAL_SEC 36
#define PIR_IGNORE_INTERVAL_SEC         10

enum running_mode : uint8_t
{
  NO_OPERATION,
  TIMER_ONLY,
  SENSOR_ONLY,
  TIMER_AND_SENSOR
}MODE_e;

void setup() {
  // put your setup code here, to run once:
Serial.begin(115200);
MODE_e = TIMER_ONLY;
pinMode(PIR_SENS_PIN, INPUT_PULLUP);
pinMode(5, INPUT_PULLUP);
}
int i = 0;
void loop() {
  // put your main code here, to run repeatedly:
  scareManager();
  delay(100);
  i++;
//  Serial.print(i++);
//  Serial.print(":\t");
}


/* Function pointer for accessing state machine functions */
typedef void(*p_stateFunction)(uint16_t * wait);

static p_stateFunction p_mainAppFunction = state_IDLE;
bool pir_fault = false;
bool nightRegime = false;

/**
 * Function shifts active regime defined delay into night time and defined delay into daytime. Has to run in defined intervals
 */
void activeRegime(bool nightTime){
  static uint16_t regimeCnt = 0;

  if(nightRegime != nightTime){
    regimeCnt++;
    if((nightRegime && regimeCnt >= TO_PASSIVE_DELAY_SEC) || (!nightRegime && regimeCnt >= TO_ACTIVE_DELAY_SEC)){
      nightRegime = !nightRegime;
      regimeCnt = 0;
      
    }
  }
  else if(regimeCnt)
    regimeCnt--;
}

static bool system_OK(void){
  //if(!lowTemperature() && !highTemperature() && !batteryUndervoltage() && !mcuUndervoltage())
    return true;
  //else
  //  return false;
}

bool isNight(void){
  if(digitalRead(5) == HIGH)
    return true;
  else
    return false;
}

static void state_IDLE(uint16_t * wait){
  // device is active only when regime is active as well as battery and mcu voltage is sufficient
  if(nightRegime && system_OK()){
    switch(MODE_e){
    case NO_OPERATION:    break;
    case TIMER_ONLY:      p_mainAppFunction = state_TIMER_ONLY;
                          break;
    case SENSOR_ONLY:     p_mainAppFunction = state_SENSOR_ONLY;
                          break;
    case TIMER_AND_SENSOR:p_mainAppFunction = state_TIMER_AND_SENSOR;
                          break;
    default:  MODE_e == TIMER_AND_SENSOR;
              break;
    }
    Serial.print(i);
    Serial.println(" Initial scare");
    *wait = 3;
  }
  else{
    digitalWrite(PIR_SW_PIN, LOW);
    *wait = 0;
    /*reset PIR fault*/
  }
}

static void state_TIMER_ONLY(uint16_t * wait){
  if(!nightRegime || !system_OK()){
    p_mainAppFunction = state_IDLE;
    Serial.print(i);
    Serial.println(" Final scare");
    return;
  }
  Serial.print(i);
  Serial.println(" Timer only scare");
  *wait = TIMER_SCARE_INTERVAL_NO_PIR_SEC;
}

static void state_SENSOR_ONLY(uint16_t * wait){
  static uint16_t scareCnt = 0;
  if(!nightRegime || !system_OK()){
    p_mainAppFunction = state_IDLE;
    scareCnt = 0;
    pir_fault = false; // done on line 165
  }
  else if(!digitalRead(PIR_SW_PIN) && !pir_fault){
    digitalWrite(PIR_SW_PIN, HIGH);
    *wait = PIR_START_UP_DLY_SEC;
  }
  else if (!pir_fault && digitalRead(PIR_SENS_PIN)){
    Serial.print(i);
    Serial.println("Sensor scare");
    *wait = PIR_IGNORE_INTERVAL_SEC; // delay for pir sensor to relax useful for weak batteries voltage fluctuates more with load
          
    // PIR fault detection based on too frequent sensored scaring
    scareCnt += MIN_PIR_SCARE_INTERVAL_SEC;
    if(scareCnt > PIR_FAULT_DETECTED_INTERVAL_SEC){
      pir_fault = true;
    }
  }

  // Pir sensor fault autoreset
  if(scareCnt){
    scareCnt--;
  }
  else if(pir_fault){
    pir_fault = false;
  }
}

static void state_TIMER_AND_SENSOR(uint16_t * wait){
  static uint16_t scareCnt = 0;
  static uint16_t waitCnt = 0;
  if(!nightRegime || !system_OK()){
    p_mainAppFunction = state_IDLE;
    scareCnt = 0;
    waitCnt = 0;
    pir_fault = false;
  }
  else if(!digitalRead(PIR_SW_PIN) && !pir_fault){
    digitalWrite(PIR_SW_PIN, HIGH);
    *wait = PIR_START_UP_DLY_SEC;
  }
  else if (!pir_fault && digitalRead(PIR_SENS_PIN)){
    Serial.print(i);
    Serial.println(" Sensor scare");
    *wait = PIR_IGNORE_INTERVAL_SEC; // delay for pir sensor to relax useful for weak batteries voltage fluctuates more with load
    waitCnt = 15 - PIR_IGNORE_INTERVAL_SEC;      
    // PIR fault detection based on too frequent sensored scaring
    scareCnt += MIN_PIR_SCARE_INTERVAL_SEC;
    if(scareCnt > PIR_FAULT_DETECTED_INTERVAL_SEC){
      pir_fault = true;
    }
  }
  else if(waitCnt == 0){
    Serial.print(i);
    Serial.println(" Timer scare");
    *wait = PIR_IGNORE_INTERVAL_SEC; // delay for pir sensor to relax useful for weak batteries voltage fluctuates more with load
    waitCnt = 15 - PIR_IGNORE_INTERVAL_SEC;
  }

  if(waitCnt)waitCnt--;

  // Pir sensor fault autoreset
  if(scareCnt){
    scareCnt--;
  }
  else if(pir_fault){
    pir_fault = false;
  }
}

void scareManager(void)
{
  // variable holding number of skipped cycles
  static uint16_t wait_cycles = 0;
  
  activeRegime(isNight());
  // function pointer runs only when wait_cycles == 0
  if(!wait_cycles){
    p_mainAppFunction(&wait_cycles);
  }
    
  
  if(wait_cycles)
    wait_cycles--;
}
