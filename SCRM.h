#ifndef SCRM_H
#define SCRM_H

#include <Arduino.h>
#include "PWRM.h"
#include "ALRM.h"
#include "config.h"

#define TO_ACTIVE_DELAY_SEC   3600
#define TO_PASSIVE_DELAY_SEC  3600

#define PIR_START_UP_DLY_SEC            60
#define TIMER_SCARE_INTERVAL_SEC        600
#define TIMER_SCARE_INTERVAL_NO_PIR_SEC 420
#define MIN_PIR_SCARE_INTERVAL_SEC      1000
#define PIR_FAULT_DETECTED_INTERVAL_SEC 3600
#define PIR_IGNORE_INTERVAL_SEC         10

enum running_mode : uint8_t
{
  NO_OPERATION,
  TIMER_ONLY,
  SENSOR_ONLY,
  TIMER_AND_SENSOR
};

void activeRegime(bool nightTime);
void scareManager_1sTask(void);

static void state_IDLE(uint16_t * wait);
static void state_TIMER_ONLY(uint16_t * wait);
static void state_SENSOR_ONLY(uint16_t * wait);
static void state_TIMER_AND_SENSOR(uint16_t * wait);

#endif
