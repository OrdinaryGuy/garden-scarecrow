#ifndef CONFIG_H
#define CONFIG_H

/* Additional functionality port */
#define EXT_GPIO_AD_PIN   A3
#define EXT_GPIO_D_PIN    3
#define EXT_NFET_SW_PIN   9

/* External switch/connector */
#define EXT_GPIO_SW1_PIN  2
#define EXT_GPIO_SW2_PIN  4

/* Digital outputs */
#define BOOST_SW_PIN    A2
#define TEMP_SW_PIN     A1
#define SOLAR_SW_PIN    13
#define PIR_SW_PIN      11
#define BUZZER_SW_PIN   10

/* Digital input */
#define PIR_SENS_PIN    12

/* Analog inputs */
#define TEMP_SENS_PIN   A0
#define SOLAR_SENS_PIN  A7
#define BATT_SENS_PIN   A6

#endif
