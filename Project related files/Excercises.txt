Links:      online C compiler           ->          https://www.onlinegdb.com/online_c_compiler
            arduino simulator           ->          https://wokwi.com/
            arduino language reference  ->          https://www.arduino.cc/reference/en/

Ex. 1: Describe code functioning
#include <stdio.h>
#include <unistd.h> // sleep function on Linux

void func1(void);
void func2(void);

int main()
{
    func1();
    
    while(1)
    {
        func2();
    }

    return 0;
}

void func1()
{
    printf("Function 1\n");
}

void func2()
{
    sleep(1);
    printf("Function 2\n");
}

Task 1: Write Arduino program updating (incrementing) time every second and write it to serial monitor. 

Ex.2: Find problem in code
#include <stdio.h>

typedef unsigned short uint16_t;

uint16_t square(uint16_t val)
{
	return val * val;
}

int main()
{
	uint16_t x = 1000;
	uint16_t result = square(x);
	printf("%d", result);
	
	return 0;
}

/**
 * Function shifts active regime defined delay into night time and defined delay into daytime. Has to run in 1s intervals
 */
bool activeRegime(bool nightTime){
  if(nightTime){ // dark
    if(nightRegime == false){
      if(++regimeCnt >= TO_ACTIVE_DELAY_SEC){
        nightRegime = true;
        regimeCnt = 0;
      }
    }
    else if(regimeCnt > 0){
      regimeCnt--;
    }
  }
  else{ // light
    if(nightRegime ){
      if(++regimeCnt >= TO_PASSIVE_DELAY_SEC){
        nightRegime = false;
        regimeCnt = 0;
      }
    }
    else if(regimeCnt > 0)
      regimeCnt--;
  }
  isActive = nightRegime;
  return nightRegime;
}

#define PIR_START_UP_DLY_SEC            60
#define TIMER_SCARE_INTERVAL_SEC        600
#define TIMER_SCARE_INTERVAL_NO_PIR_SEC 420
#define MIN_PIR_SCARE_INTERVAL_SEC      1000
#define PIR_FAULT_DETECTED_INTERVAL_SEC 3600
#define PIR_IGNORE_INTERVAL_SEC         10
void scareManager(){
  static uint16_t scareCounter = 0;
  static uint16_t scareTimer = 0;
  static uint8_t pirScareDelay = 0;
  static uint8_t pirIgnoreCnt = 0;
  static bool pir_fault = false;
  
  // device is active only when regime is active as well as battery and mcu voltage is sufficient
  if(activeRegime(isNight()) && !batteryUndervoltage() && !mcuUndervoltage()){

    // PIR voltage unsufficient for operation so only timer scaring
    if(pirUndervoltage()){
      // Power down PIR sensor if active
      if(digitalRead(PIR_SW_PIN)) 
        digitalWrite(PIR_SW_PIN, LOW);

      // Scare timing
      if(++scareTimer >= TIMER_SCARE_INTERVAL_NO_PIR_SEC){
        scare(SHORT_SCARE);
        scareTimer = 0;
      }
    // timer scaring and sensor scaring 
    }else{
      // PIR sensor start up, sensor must be on and ignore cnt greater than 0
      if(digitalRead(PIR_SW_PIN)){
        if(pirIgnoreCnt)
          pirIgnoreCnt--;
      }else{
        digitalWrite(PIR_SW_PIN, HIGH);
        pirIgnoreCnt = PIR_START_UP_DLY_SEC;
      }

      if(!pirIgnoreCnt){
        if(!pir_fault && digitalRead(PIR_SENS_PIN)){
          scare(LONG_SCARE);
          scareTimer = 0;
          pirIgnoreCnt += PIR_IGNORE_INTERVAL_SEC; // delay for pir sensor to relax useful for weak batteries voltage fluctuates more with load
          
          // PIR fault detection based on too frequent sensored scaring
          scareCounter += MIN_PIR_SCARE_INTERVAL_SEC;
          if(scareCounter > PIR_FAULT_DETECTED_INTERVAL_SEC){
            pir_fault = true;
          }
            
        }else if(scareTimer >= TIMER_SCARE_INTERVAL_SEC){
          scare();
          scareTimer = 0;
          pirIgnoreCnt += PIR_IGNORE_INTERVAL_SEC; // delay for pir sensor to relax
        }
      }
      scareTimer++;

      // decrementing scareCnt and reset pir_fault flag
      if(scareCounter) scareCounter--;
      else pir_fault = false;
      // error logging
      pirSensorFalut = pir_fault;
    }

  } else { // PIR turn off
    digitalWrite(PIR_SW_PIN, LOW);
    scareCounter = 0;
    scareTimer = 0;
    pirScareDelay = 0;
    pirIgnoreCnt = 0;
    pir_fault = false;
  }
}
